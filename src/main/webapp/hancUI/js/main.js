var rootURL = "rws/services";

getAllLinksAndTags();

function getAllLinksAndTags(){
	getAllTags();
	getAllLinks();
}

$(document).ready(function() {
	$( "#newLinkForm" ).submit(function( event ) {
		saveNewURLWithInfoFromForm()
		event.preventDefault();
	});
});

function getAllTags() {
	console.log('getAllTags');
	$.ajax({
		type: 'GET',
		url: rootURL+'/getAllTags',
		dataType: "json", // data type of response
		success : function(data) {
			$("#tag-list").empty();
			if(data == "fail") {
				alert('error');
			}
			else {
				$.each(data, function (i) {
					$("#tag-list").append( "<a href='#' onclick='getLinkForTag(event)'>" + data[i] + "\t</a>");
				})
			}
		}
	});
}


function getLinkForTag(e){
	var target = e.target || e.srcElement;
	var tag = target.innerHTML;
	$.ajax({
		type: 'GET',
		url: rootURL+'/get/' + tag,
		dataType: "json", // data type of response
		success : function(data) {
			displayBookmarks(data);
		}
	});
}

function displayBookmarks(data){
	if(data == "fail") {
		alert('error');
	}
	else {
		$("#bookmark-list").empty();
		$.each(data, function (i) {
			$("#bookmark-list").append( "<br><a class='text-info h5' href='"+ data[i].url + "'>" + data[i].url + "</a>");
			$("#bookmark-list").append( "<br><h5>" + data[i].description + "</h5>" );
			$.each(data[i].tags, function (j) {
				$("#bookmark-list").append( "<small class='text-muted'>" + data[i].tags[j] + "\t</small>" );
			})
			$("#bookmark-list").append( "<div class='bookmark-widget'></div>" );
		})
	}
}

function getAllLinks() {
	$.ajax({   
		method: "GET",   
		url: rootURL+"/getAllURL",   
		dataType: "json", // data type of response   
		success : function(data)                 
			{ displayBookmarks(data); }        
	});
} 

function saveNewURLWithInfoFromForm() {
	$.ajax({   
		method: "POST",   
		url: rootURL+"/save",   
		data: $('form').serialize(), // data type of response   
		success : function(data)                 
			{
				$( "#bookmark-success" ).append("<p class='textsuccess'>Bookmark saved successfully</p>");  
				$( 'form' ).trigger("reset");
			}        
	});
}


