<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean id="globalHelper" class="com.celestial.dsb.core.ApplicationScopeHelper" scope="application"/>

<!DOCTYPE html>
<html>
    <head>
        <title>DSB</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"></meta>
        <link rel="stylesheet"
              href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
              integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
              crossorigin="anonymous">
        <link rel="stylesheet" type="text/css" href="hancUI/css/main.css" />
        <script
        src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
        <script
            src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
            integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
        crossorigin="anonymous"></script>

        <script>
            $(function () {
                $("#newLinkForm").load("hancUI/presentation/NewLinkForm.html");
            });
        </script>
        <script src="hancUI/js/main.js"></script>
    </head>
    <body>
        <%
            String dbStatus = "DB NOT CONNECTED";

            globalHelper.setInfo("Set any value here for application level access");
            boolean connectionStatus = globalHelper.bootstrapDBConnection();

            if (connectionStatus)
            {
                dbStatus = "You have successfully connected the Deutsche Bank Bookmarking Service";
            }
        %>
        <h2><%= dbStatus%></h2>
        <%
            if (connectionStatus)
            {
        %>

        <ul class="nav nav-tabs" id="myTab" role="tablist">
            <li class="nav-item"><a class="nav-link fade in active" id="home-tab"
                                    data-toggle="tab" href="#home" role="tab" aria-controls="home"
                                    aria-selected="true" onclick="getAllLinksAndTags()">All Bookmarks</a></li>
            <li class="nav-item"><a class="nav-link" id="newBookmark-tab"
                                    data-toggle="tab" href="#newBookmark" role="tab" aria-controls="newBookmark"
                                    aria-selected="false">New Bookmark</a></li>
        </ul>
        <div class="tab-content" id="myTabContent">
            <div class="tab-pane fade in active" id="home" role="tabpanel"
                 aria-labelledby="home-tab">
                <div class="tab-content">
                    <div id="bookmark-panel" class=col-sm-7>
                        <h3 id="bookmark-header">All Bookmarks </h3>
                        <div id="bookmark-list"></div>
                    </div>
                    <div class=col-sm-3>
                        <a href="#" onclick='getAllLinks()' id="tag-allCategories">All Categories</a>
                        <div id="tag-list"></div>
                    </div>

                </div>
            </div>
            <div class="tab-pane fade" id="newBookmark" role="tabpanel"
                 aria-labelledby="newBookmark-tab">
                <div id="newLinkForm" class="col-sm-10"></div>
            </div>
        </div>
        <%
            }
        %>

    </body>
</html>
